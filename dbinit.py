import sqlite3

con = sqlite3.connect("bot.db")
cur = con.cursor()

cur.execute(
    """CREATE TABLE leaderboard
        (user INTEGER PRIMARY KEY, points INTEGER NOT NULL)"""
)
con.commit()
cur.close()
