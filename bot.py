"""
Trivia Bot for ECT
(c) 2021 Brian Blevins
Licensed under the GNU Affero General Public License, version 3
"""

import discord
from discord.commands.commands import Option
from discord.ext import commands
import json
from discord.ext.commands.core import has_permissions

import aiohttp
import asyncio
import random
import sqlite3

# Define all the configuration variables
with open("config.json") as config_f:
    config = json.load(config_f)

with open("questions.json") as question_f:
    questions_json = json.load(question_f)

token = config["token"]
questions = questions_json["questions"]
bot = discord.Bot()
is_playing = {}


class Database(object):
    def __enter__(self):
        self.conn = sqlite3.connect("bot.db")
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.conn.close()

    def __call__(self, query, values=None):
        c = self.conn.cursor()
        try:
            if values:
                result = c.execute(query, values)
            else:
                result = c.execute(query)
            self.conn.commit()
        except Exception as e:
            result = e
        return result


# Class for intaking all the trivia responses
class Responses:
    users = []

    def __init__(self, users=users):
        self.users = users

    async def update(self, correct_response, response, user):
        # Check if the response is correct
        if response == correct_response:
            self.users.append(user)
            return 2


# Creates the buttons on the trivia message
class TriviaButtons(discord.ui.View):
    responses = {}

    async def on_timeout(self):
        for child in self.children:
            child.disabled = True
        self.clear_items()

    # Add the buttons
    @discord.ui.button(label="A", style=discord.ButtonStyle.primary)
    # Each callback sets the users response
    async def a_callback(self, button, interaction, responses=responses):
        responses[interaction.user] = "a"

    @discord.ui.button(label="B", style=discord.ButtonStyle.primary)
    async def b_callback(self, button, interaction, responses=responses):
        responses[interaction.user] = "b"

    @discord.ui.button(label="C", style=discord.ButtonStyle.primary)
    async def c_callback(self, button, interaction, responses=responses):
        responses[interaction.user] = "c"

    @discord.ui.button(label="D", style=discord.ButtonStyle.primary)
    async def d_callback(self, button, interaction, responses=responses):
        responses[interaction.user] = "d"


class Leaderboard:
    lead_dict = {}
    users = {}

    def __init__(self, users=users, lead_dict=lead_dict):
        self.users = list(lead_dict.keys())

    # Adds x points to the user
    async def update(self, user, points):
        self.lead_dict[user] = self.lead_dict.get(user, 0) + points
        # Update the global leaderboard
        with Database() as db:
            db(
                """INSERT INTO leaderboard VALUES (?, ?)
            ON CONFLICT DO UPDATE SET points=points+?""",
                (int(user.id), int(points), int(points)),
            )

    async def points(self, user):
        return self.lead_dict[user]

    async def sort(self):
        # Sorts the leaderboard list by the amount of points each user has.
        sort_list = sorted(self.lead_dict.items(), key=lambda x: x[1], reverse=True)
        self.lead_dict = dict(sort_list)
        self.users = list(self.lead_dict.keys())


# Gets the json response from a given website. Currently only used for quotes.
async def get_json_url(site_url):
    async with aiohttp.ClientSession() as session:
        async with session.get(site_url) as response:
            return await response.json()


async def quote_get():
    return await get_json_url("https://api.quotable.io/random")


async def make_leader_embed(embed: discord.Embed, leaderboard):
    user_embed = ""
    points_embed = ""
    if leaderboard.users:
        for user in leaderboard.users:
            points = await leaderboard.points(user)
            user_embed += f"{user.display_name}\n"
            points_embed += f"{points}\n"
    else:
        user_embed = "*** ***"
        points_embed = "*** ***"

    embed.add_field(name="User", value=f"{user_embed}", inline=True)
    embed.add_field(name="Points", value=f"{points_embed}", inline=True)


@bot.slash_command(name="leaderboard", description="Display the global trivia leaderboard")
async def leaderboard(ctx):
    with Database() as db:
        result = db("SELECT * FROM leaderboard ORDER BY points DESC")
        result = result.fetchall()

    embed = discord.Embed(title="Global Leaderboard")
    user_embed = ""
    points_embed = ""
    for user_id, points in result:
        user = await bot.fetch_user(user_id)
        user_embed += f"{user.display_name}\n"
        points_embed += f"{points}\n"

    embed.add_field(name="User", value=user_embed)
    embed.add_field(name="Points", value=points_embed)
    await ctx.respond(embed=embed)


@bot.slash_command(description="Quote of the day")
async def qotd(ctx):
    quote_j = await quote_get()
    quote = quote_j["content"]
    author = quote_j["author"]
    embed = discord.Embed(title="Quote of the day", description=f"{quote} -{author}")
    await ctx.respond(embed=embed)


@bot.slash_command(description="Play some trivia!")
async def trivia(
    ctx,
    timeout: Option(int, "Time per question", required=False, default=30),
    rounds: Option(int, "Amount of questions in a session", required=False, default=10),
):
    # Initialize command-specific variables
    rounds_played = 0
    leaderboard = Leaderboard()
    global is_playing

    # Check if the Discord channel is already playing a round of trivia.
    # If it is, prevent a new one from starting.
    if is_playing.get(ctx.interaction.channel, 0) == 1:
        await ctx.send("You cannot start a game while another is running!", delete_after=5)
        return 1

    # Set the channels "is_playing" to 1, to prevent someone from starting
    # a new round in the same channel.
    is_playing = {ctx.interaction.channel: 1}

    # Initialize a message we can use (edit) for the entire game.
    embed = discord.Embed(title="Welcome to trivia!", description="Let's get into it.")
    trivia_message = await ctx.respond(embed=embed)
    await asyncio.sleep(2)
    # Play for the defined amount of rounds, defaults to 10
    while rounds_played < rounds:
        rounds_played += 1
        responses = Responses()
        question = random.choice(questions)

        embed = discord.Embed(title="Trivia", description=question["question"])
        embed.add_field(name="A", value=question["a"], inline=False)
        embed.add_field(name="B", value=question["b"], inline=False)
        if "c" in question:
            embed.add_field(name="C", value=question["c"], inline=False)
        if "d" in question:
            embed.add_field(name="D", value=question["d"], inline=False)

        view = TriviaButtons(timeout=timeout)
        await trivia_message.edit_original_message(embed=embed, view=view)
        await view.wait()

        for user, response in view.responses.items():
            update_response = await responses.update(question["correct_answer"], response, user)
            if update_response == 2:
                await leaderboard.update(user, 200)

        correct_letter = question["correct_answer"].upper()
        timeout_embed = discord.Embed(title="Times up!")
        timeout_embed.add_field(
            name="The correct answer was",
            value=f"{correct_letter}: {question[correct_letter.lower()]}",
        )

        if responses.users:
            await leaderboard.update(responses.users[0], 300)
            timeout_embed.add_field(
                name="First correct answer:",
                value=responses.users[0].display_name,
                inline=False,
            )
        else:
            timeout_embed.add_field(name="No correct answers!", value="Better luck next time.", inline=False)
        await trivia_message.edit_original_message(embed=timeout_embed, view=None)

        await asyncio.sleep(4)
        if rounds_played < rounds:
            board_embed = discord.Embed(title="Leaderboard")
            await leaderboard.sort()
            await make_leader_embed(board_embed, leaderboard)
            await trivia_message.edit_original_message(embed=board_embed)

        await asyncio.sleep(7)
        responses.users.clear()
        TriviaButtons.responses.clear()

    embed = discord.Embed(title="Game over!")
    embed.add_field(name="The winner is", value=leaderboard.users[0], inline=False)
    await make_leader_embed(embed, leaderboard)
    await trivia_message.edit_original_message(embed=embed)

    is_playing[ctx.interaction.channel] = 0


@bot.slash_command(description="Purge messages. Requires Manage Messages.")
@has_permissions(manage_messages=True)
async def purge(ctx, amount: Option(int, "Amount of messages to purge", required=False, default=1000)):
    deleted = await ctx.channel.purge(limit=amount)
    await ctx.respond(f"{len(deleted)} messages deleted.", delete_after=10)


bot.run(token)
