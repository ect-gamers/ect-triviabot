import json
import sys
keep_going = True
while keep_going == True:
    responses = {}
    print("Question?")
    question = sys.stdin.read()
    print("A?")
    responses["a"] = sys.stdin.read()
    print("B?")
    responses["b"] = sys.stdin.read()
    print("C?")
    responses["c"] = sys.stdin.read()
    print("D?")
    responses["d"] = sys.stdin.read()
    correct_answer = input("Correct answer?")
    new_question = {}
    new_question["question"] = question
    for letter, value in responses.items():
        new_question[letter] = value
    new_question["correct_answer"] = correct_answer
    with open("questions.json", "r+") as file:
        data = json.load(file)
        data["questions"].append(new_question)
        file.seek(0)
        json.dump(data, file, indent=4)
    go_again = input("Add another question? y/n")
    if go_again.casefold() == "n":
        keep_going = False
    
