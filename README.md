# ECT Triviabot

A Discord Trivia Bot written for ECT class

## Install
This project has 2 dependencies:
* Python 3.7 or above
* [Pycord](https://github.com/Pycord-Development/pycord)

To install, run `python3 dbinit.py`

Afterwards, modify `config.json`, replacing TOKENHERE with your bot's token.

Finally, the bot is run by running `python3 bot.py`

## LICENSE
This project is licensed under the GNU Affero General Public License version 3. A copy is attached.
